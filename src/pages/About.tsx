const About = () => {
  return (
    <>
      <h1>About</h1>
      <h2>This is a simple example of React, Redux, Tailwind, Typescript store.</h2>
      <p>Incrementing bit by bit for study</p>
    </>
  )
}

export default About;
