import React, { useState, useRef } from 'react';

const questions = [
  {
    "id": 1,
    "description": "Question 1 description aaa",
    "title": "Question 1 title"
  },
  {
    "id": 2,
    "description": "Question 2 description bbb",
    "title": "Question 2 title"
  },
  {
    "id": 3,
    "description": "Question 3 description ccc",
    "title": "Question 3 title"
  }
]

const SupportQuestionList = () => {
  const [searchTerm, setSearchterm] = useState<string>('');

  return (
    <div>
      <input type="text" className='mb-10 p-4' name="name" placeholder='Search your question here' value={searchTerm} onChange={(e) => {setSearchterm(e.currentTarget.value)}} />
      {questions.filter((question: { [s: string]: unknown; } | ArrayLike<unknown>) => {
        if (searchTerm == '') {
          return question
        } else if (Object.values(question).join('').toLowerCase().includes(searchTerm.toLowerCase())) {
          return question
        }
      }).map((question: any, key: number) => {
        return (
          <div key={key} className="p-5 bg-white mb-5 rounded-md">
            <h3>{question.title}</h3>
            <p>{question.description}</p>
          </div>
        )
      })}
    </div>
  )
}

export default SupportQuestionList
