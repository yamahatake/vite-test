import { Link } from 'react-router-dom'
import { useSelector } from 'react-redux'

const ProductComponent = () => {
  const products = useSelector((state) => state.allProducts.products)

  const renderList = products.map((product: { id: number; title: string; price: number; description: string; category: string; image: string }) => {
    const { id, title, price, image } = product

    return (
      <div key={id} className="p-5 bg-white rounded-md hover:scale-105 transition ease-in-out">
        <Link to={`/product/${id}`}>
          <figure className='mb-5 h-[200px] flex justify-center items-center'>
            <img className="max-h-[200px]" src={image} alt={title} />
          </figure>
          <h5 className='leading-5'>{title}</h5>
          <h2>$ {price}</h2>
        </Link>
      </div>
    )
  })

  return (
    <div>
      <div className='grid grid-flow-row-dense lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4'>
        {renderList}
      </div>
    </div>
  )
}

export default ProductComponent
