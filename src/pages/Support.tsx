import SupportQuestionList from '../components/SupportQuestionList'

const Support = () => {

  return (
    <>
      <h1>Support</h1>      
      <div>
        <label>
          <h2>What do you need help with?</h2>
        </label>        
      </div>
      <SupportQuestionList />
    </>
  )
}

export default Support
