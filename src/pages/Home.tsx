import ProductListing from '../components/products/ProductListing'

const Home = () => {
  return (
    <>
      <ProductListing />
    </>
  )
}

export default Home;
