import { Link } from "react-router-dom"

const Footer = () => {
  return (
    <div className="container mx-auto p-5 max-w-5xl px-5 text-white text-xs text-center">
      <p>© 2022 Corporation.  All rights reserved. All trademarks are property of their respective owners in the US and other countries. VAT included in all prices where applicable</p>
      <div className="mt-5">
        <Link to="/" className="mx-4">Privacy Policy</Link>|
        <Link to="/" className="mx-4">Legal</Link>|
        <Link to="/" className="mx-4">Refunds</Link>|
        <Link to="/" className="mx-4">Cookies</Link>
      </div>
    </div>
  )
}

export default Footer
