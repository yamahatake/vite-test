import React, { useEffect, useState } from "react";
import axios from "axios";
import { useParams } from "react-router-dom"
import { Link } from 'react-router-dom'


const Category = () => {
  const { category } = useParams()
  const [products, setProducts] = useState<Array>([])
  
  const fetchCategory = async () => {
    const response = await axios
      .get(`https://fakestoreapi.com/products/category/${category}`)
      .catch((err) => {
        console.log("Err", err);
      });
      setProducts(response.data)
  };

  useEffect(() => {
    fetchCategory();
  }, []);

  const renderList = products.map((product: { id: number; title: string; price: number; description: string; category: string; image: string }) => {
    const { id, title, price, image } = product

    return (
      <div key={id} className="p-5 bg-white rounded-md hover:scale-105 transition ease-in-out">
        <Link to={`/product/${id}`}>
          <figure className='mb-5 h-[200px] flex justify-center items-center'>
            <img className="max-h-[200px]" src={image} alt={title} />
          </figure>
          <h5 className='leading-5'>{title}</h5>
          <h2>$ {price}</h2>
        </Link>
      </div>
    )
  })
  
  return (
    <div>
      <h1 className="capitalize">{category}</h1>
      <div className='grid grid-flow-row-dense lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-4'>
        {renderList}
      </div>
    </div>
  )
}

export default Category
