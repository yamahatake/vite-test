import React, { useEffect } from "react"
import { useParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux";
import { selectedProduct, removeSelectedProduct } from '../../redux/actions/productActions';
import { ActionTypes } from "../../redux/constants/action-types";
import { Link } from 'react-router-dom'
import axios from "axios"

const ProductDetail = () => {
  const product = useSelector((state) => state.product)
  const {image, title, price, category, description} = product
  const { productId } = useParams()
  const dispatch = useDispatch();
  
  const fetchProductDetail = async () => {
    const response = await axios
      .get(`https://fakestoreapi.com/products/${productId}`)
      .catch((err) => {
        dispatch( {
          type: ActionTypes.PRODUCTS_ERROR,
          payload: err,
        })
        console.log("Err", err);
      });
    
    dispatch(selectedProduct(response.data))
  }

  useEffect(() => {
    if (productId && productId !== "") fetchProductDetail()
    return () => {
      dispatch(removeSelectedProduct())
    }
  },[productId])

  let productHTML
  if (Object.keys(product).length === 0) {
    productHTML = <div>LOADING</div>
  } else {
    productHTML = (
      <>
        <div className="flex-1 w-2/4 pr-10">
          <figure className='mb-5 flex justify-center items-center'>
            <img className="w-full" src={image} alt={title} />
          </figure>
        </div>
        <div className="flex-1">
          <h1>{title}</h1>
          <div className="mb-3 text-4xl">$ {price}</div>
          <Link to={`/category/${category}`} className="bg-slate-500 hover:bg-slate-600 px-3 py-1 mt-5 rounded text-white capitalize">{category}</Link>
          <p className="my-5">{description}</p>
          <Link to={`/`} className="bg-cyan-500 hover:bg-cyan-600 px-4 py-2 rounded text-white text-2xl font-semibold mt-5">Add to cart</Link>
        </div>
      </>
    )
  }
  
  return (
    <div className="flex">
      {productHTML}
    </div>
  )
}

export default ProductDetail
