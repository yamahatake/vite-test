import { Routes, Route, Link, Outlet } from "react-router-dom"
import logoURL from "public/images/logo.svg"
import './App.css'

// PAGES
import Home from './pages/Home'
import About from './pages/About'
import Store from './pages/Store'
import Support from './pages/Support'
import Page404 from './pages/Page404'

// PRODUCTS
import ProductDetail from './components/products/ProductDetail'

// CATEGORY
import Category from './components/Category'

// ELEMENTS
import Footer from './components/Footer'

const App = () => {
  return (
    <div className="main bg-zinc-100">
      <>
        <Routes>
          <Route element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="/store" element={<Store />} />
            <Route path="/product/:productId" element={<ProductDetail />} />
            <Route path="/category/:category" element={<Category />} />
            <Route path="/about" element={<About />} />
            <Route path="/support" element={<Support />} />
            <Route path="*" element={<Page404 />} />
          </Route>
        </Routes>
      </>
    </div>
  )
}

const Layout = () => {
  return (
    <>
      <nav className="shadow-lg bg-white font-sans">
        <div className="container mx-auto flex items-start content-center max-w-5xl px-5">
          <Link className="mr-10" to="/">
            <img className="pt-2 pb-2 pr-5" src={logoURL} alt="logo" />
          </Link>
          <ul className="flex">
            <li>
              <Link className="p-5 font-semibold" to="/store">Store</Link>
            </li>
            <li>
              <Link className="p-5 font-semibold" to="/about">About</Link>
            </li>
            <li>
              <Link className="p-5 font-semibold" to="/support">Support</Link>
            </li>
          </ul>
        </div>
      </nav>
      <div className="container mx-auto pt-10 pb-10 max-w-5xl px-5 min-h-[800px]">
        <Outlet />
      </div>
      <div className="bg-zinc-800">
        <Footer />
      </div>
    </>
  );
}

export default App;